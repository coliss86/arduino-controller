#!/usr/bin/perl -w
# activate / deactivate pin
# doesn't handle the port type (relay) as the arduino take care of it
# when the arduino starts, all pin are low but for relay they're high

use strict;
use warnings;
use Dir::Self;
use List::Util qw(min max);
use Sys::CpuLoad qw(load);
use Filesys::Df qw(df);
use Math::Round qw(round);
use lib File::Spec->catdir( __DIR__, '.');
use Arduino;

if (scalar(@ARGV) > 0) {
    if ( $ARGV[0] eq "-d" ) {
        set_debug(1);
    } elsif ($ARGV[0] eq "-help" || $ARGV[0] eq "--help") {
        print_usage($0);
    }
}

$SIG{'INT'} = \&signal_handler;
$SIG{'TERM'} = \&signal_handler;

my $cfg = load_config();
my $portName = $cfg->param('tty');
die_cleanly("port not defined") unless (defined $portName);

# locks
my $filenames_lock = $cfg->param('filenames_lock');
die_cleanly("filenames_lock not defined") unless (defined $filenames_lock);
my $filename_lock = "$filenames_lock$$";

my $fs_to_monitor = $cfg->param('fs_to_monitor');
die_cleanly("file system to watch not defined") unless (defined $fs_to_monitor);

my @loads = load();
my $value1 = load_to_log_percent($loads[0]);
my $value5 = load_to_log_percent($loads[1]);
my $df = df($fs_to_monitor, 1024)->{per};
log_debug("load1 : $loads[0] => $value1 %, load5 : $loads[1] => $value5 %, fs ($fs_to_monitor) $df %");

check_lock($filename_lock, $filenames_lock, 1);

my $port = open_serial($portName);

$port->write(".");
empty_serial_buffer();
$port->write("stats $value1 $value5 $df.");

empty_serial_buffer();

exit_cleanly(0);

#####################
# convert load value to percent with a custom formula
sub load_to_log_percent {
    my $load = shift;
    return max(min(round((log(0.01 + $load*$load)/log(10)+2)/4 * 100), 100), 0);
}

#####################
# Print help message
# @param $program_name
sub print_usage {
    print "$0 [-p dev]
        <dev>   - device [/dev/ttyACM0]\n";

    exit_cleanly(1);
}

