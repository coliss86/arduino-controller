#!/usr/bin/perl -w
# activate / deactivate pin
# doesn't handle the port type (relay) as the arduino take care of it
# when the arduino starts, all pin are low but for relay they're high

use strict;
use warnings;
use Dir::Self;
use lib File::Spec->catdir( __DIR__, '.');
use Arduino;

my $status = "";

if (scalar(@ARGV) == 0 || $ARGV[0] eq "-help" || $ARGV[0] eq "--help") {
    print_usage($0);
}

$SIG{'INT'} = \&signal_handler;
$SIG{'TERM'} = \&signal_handler;

my $cfg = load_config();
my $portName = $cfg->param('tty');
die_cleanly("port not defined") unless (defined $portName);

# locks
my $filenames_lock = $cfg->param('filenames_lock');
die_cleanly("filenames_lock not defined") unless (defined $filenames_lock);
my $filename_lock = "$filenames_lock$$";

my $arg = 0;
if (scalar(@ARGV) == 4 || scalar(@ARGV) == 3) {
    $arg++;
    $portName = $ARGV[$arg];
    $arg++;
}

my $pin = 0;
my $state = "";
if ($ARGV[$arg] eq "status") {
    $status = "status";
    print "Status\n";
} else {
    $pin = $ARGV[$arg];
    $arg++;
    $state = $ARGV[$arg];

    if ($pin =~ /^-?\d+\z/) {
# $pin is an integer
        if ($pin < $cfg->param('port_start') || $pin > $cfg->param('port_end')) {
            print "Pin out of range : [".$cfg->param('port_start')." - ".$cfg->param('port_end')."]\n";
            print_usage($0);
        }
    } else {
# $pin is a pin name
        my $i = 0;
        my $found = 0;
        for ($i = $cfg->param('port_start'); $i <= $cfg->param('port_end'); $i++) {
            my $value = $cfg->param("port$i");
            if (defined $value && $value eq $pin) {
                $pin = $i;
                $found = 1;
                last;
            }
        }
        if ($found == 0) {
            print "unknown pin name : $pin\n";
            exit_cleanly(1);
        }
    }

    if ($state ne 'high' && $state ne 'low' && $state ne 'off' && $state ne 'on' && $state ne 'toggle') {
        print_usage($0);
    }
}

check_lock($filename_lock, $filenames_lock, 1);

my $port = open_serial($portName);

my $prev = "";
my $found = 0;
$port->write("s.");
serial_search( sub {
    my $st = shift;
    if ($status eq "status" && $st =~ m/.* => (\d)( \(R\))?/) {
        print "$st\n";
    }
# format expected :   2 => 0
    if ($st =~ m/.*$pin => (\d)( \(R\))?/) {
        $prev = $1;
        $found = 1;
    }
});

if ($status eq "status") {
    exit_cleanly(0);
}

if ($found == 0) {
    die_cleanly("Pin number $pin not found in the arduino");
}

my $send;
if ($state eq 'toggle') {
    if ($prev eq "0") {
        $send = "1";
    } else {
        $send = "0";
    }
} else {
    if ($state eq 'high' || $state eq 'on') {
        $send = "1";
    } else {
        $send = "0";
    }
}

$port->write("pin $pin $send.");

empty_serial_buffer();

exit_cleanly(0);

#####################
# Print help message
# @param $program_name
sub print_usage {
    print "$0 [-p dev] (<pin> <state> | status)
        <dev>   - device [/dev/ttyACM0]
        status  - retrieve the current status of the outputs
        <pin>   - pin number
        <action> - low|off / high|on / toggle\n";

    exit_cleanly(1);
}

