#!/usr/bin/perl -w
# read the temperature / pressure from the arduino and stores it the database

use strict;
use warnings;
use Date::Manip;
use Dir::Self;
use lib File::Spec->catdir( __DIR__, '.');
use Arduino;

# output
my $output = 0;

if ($#ARGV == 0) {
    if ( $ARGV[0] eq "-d" ) {
        set_debug(1);
    } elsif ( $ARGV[0] eq "-o" ) {
        $output = 1;
    } else {
        print_usage($0);
    }
}

$SIG{'INT'} = \&signal_handler;
$SIG{'TERM'} = \&signal_handler;

my $cfg = load_config();
my $portName = $cfg->param('tty');
die_cleanly("port not defined") unless (defined $portName);

# influxdb
my $influxdb_host = $cfg->param('influxdb_host');
die_cleanly("influxdb_host not defined") unless (defined $influxdb_host);
my $influxdb_port = $cfg->param('influxdb_port');
die_cleanly("influxdb_port not defined") unless (defined $influxdb_port);
my $influxdb_org = $cfg->param('influxdb_org');
die_cleanly("influxdb_org not defined") unless (defined $influxdb_org);
my $influxdb_bucket = $cfg->param('influxdb_bucket');
die_cleanly("influxdb_bucket not defined") unless (defined $influxdb_bucket);
my $influxdb_token = $cfg->param('influxdb_token');
die_cleanly("influxdb_token not defined") unless (defined $influxdb_token);

# locks
my $filenames_lock = $cfg->param('filenames_lock');
die_cleanly("filenames_lock not defined") unless (defined $filenames_lock);
my $filename_lock = "$filenames_lock$$";

# temperature dir
my $temperature_dir = $cfg->param('temperature_dir');
die_cleanly("temperature_dir not defined") unless (defined $temperature_dir);

# sensors map
my @SENSORS = ();
my @SENSORS_NAMES = ();

my %config = $cfg->vars();
my $value ="";
foreach my $key (keys %config) {
    if ($key =~ /.*sensor.*/) {
        $value = $config{$key};
        our ($rom, $name) = split(':',$value);
        $rom =~ s/^\s+|\s+$//g;
        push (@SENSORS, $rom);
        $name =~ s/^\s+|\s+$//g;
        push (@SENSORS_NAMES, $name);
    }
}

check_lock($filename_lock, $filenames_lock, 1);

# Set up the serial port
my $port = open_serial($portName);

my %data = ();
my $size = 0;
my $cpt = 0;
# read the temperature
do {
    $port->write(".");
    empty_serial_buffer();
    $port->write("t.");
    serial_search( sub {
        my $st = shift;
        if ($st =~ m/ROM \[ (.*) \].*= (.*) .*/) {
                $data{$1} = $2;
            log_debug("temp : $1 = $2");
        }
    });

    $size = keys %data;
    $cpt++;
} while ($cpt < 2 && $size == 0);

die_cleanly("No response or no temperature found") unless ($size);

# read the pressure
$port->write("p.");
serial_search( sub {
    my $st = shift;
    if ($st =~ m/Pressure = (.*) .*/) {
        $data{'pressure'} = $1;
        log_debug("pressure : $1");
    }
});

my $tempbuf = printable(%data);

if ($output) {
    print_temp($tempbuf);
} elsif (!get_debug()) {
    update_influxdb(%data);
    log_temp($tempbuf);
} else {
    print "not updating databases\n";
}

exit_cleanly(0);

#####################
# Print help message
# @param $program_name
sub print_usage {
    print "$0 [-d] [-o]
        -d : debug output and do not update database
        -o : print read value to stdout and do not update database\n";

    exit (1);
}

#####################
# return a string with temperature
# @param %data
sub printable {
    my @data = shift;

    my $string;
    my $i = 0;
    for ($i = 0; $i < scalar(@SENSORS_NAMES); $i++) {
        if (defined $data{$SENSORS[$i]}) {
            $string .= $SENSORS_NAMES[$i]."=".$data{$SENSORS[$i]}." - ";
        }
    }
    return $string;
}

#####################
# log temperature to file
# @param @string
sub log_temp {
    my $string = shift;

    my $date = UnixDate(ParseDate('today'), "%Y-%m-%d");
    my $file = $temperature_dir."temperature.".$date.".log";
    open (LOGFILE, ">>$file");
    my $ts = UnixDate(ParseDate('now'), "%Y-%m-%d %H:%M:%S");
    print LOGFILE "$ts : $string\n";
    close (LOGFILE);
}

#####################
# update influxdb
# @param %data
sub update_influxdb {
    my @data = shift;

    my $data_bin;

    for (my $i = 0; $i < scalar(@SENSORS_NAMES); $i++) {
        if (defined $data{$SENSORS[$i]}) {
	        $data_bin .= "$SENSORS_NAMES[$i] val=$data{$SENSORS[$i]}\n";
        }
    }
    $data_bin = substr $data_bin, 0, -1;
    exec_cmd("curl -s -X POST \"http://${influxdb_host}:${influxdb_port}/api/v2/write?org=${influxdb_org}&bucket=${influxdb_bucket}&precision=s\" --header \"Authorization: Token ${influxdb_token}\" --header \"Content-Type: text/plain; charset=utf-8\" --header \"Accept: application/json\" --data-binary \"${data_bin}\"");
}

#####################
# print temperature 
# @param @string
sub print_temp {
    my $string = shift;
    $string =~ s/ - /\n/g;
    print $string;
}

