# Arduino controller

Set of programs to control easily from a perl script an Arduino to switch on or off a relay card, retrieve temperature from DS18B20 sensors and even display the load average on vumeter.

Find out more info (in french) at [my homepage](https://www.geekeries.fun/realisations_arduino_start.html)

The source repository is composed of two parts : 
 - `constroller/` : perl program to run on a linux based distro. It communicates through the emulated serial port with the Arduino with a simple text protocol to retrieve state, temperature and switch on or off outputs. The Arduino listens for order and executes them.
 - `sketch/` : C program for the Arduino : it listens on the serial port for commands sent from the perl script or through the prompt 

## Installation

### Controller

Install the following for the perl script : 

 - `libdir-self-perl` : to load local module
 - `libdevice-serialport-perl` : to open a serial communication
 - `libconfig-simple-perl` : to read config file
 - `libfilesys-df-perl` : to read file system space remaining
 - `libsys-cpuload-perl` : to read cpu load

1. On a Debian based distribution, 
```
$ sudo apt install libdir-self-perl libdevice-serialport-perl libconfig-simple-perl libfilesys-df-perl libsys-cpuload-perl
```

2. Create the configuration file `/etc/arduino.cfg`
```ini
# device path
tty=/dev/ttyArduino

# temperature dir
temperature_dir=/var/lib/temperature/

# lock file
filenames_lock=/var/run/arduino/arduino_lock.

# influxdb conf
influx_host=localhost
influx_port=8086
influx_database=sensors

# arduino pin configuration
port_start=5
port_end=8

# pin name
# example : port1=name

port6=wifi
port7=light
port8=webcam

# temperature
sensor1=28 0A 4C 3B 04 00 00 5B : ext
sensor2=28 06 AD 33 04 00 00 2D : garage
sensor4=28 65 DC 33 04 00 00 17 : server
sensor6=pressure : pressure

# filesystem to monitor
fs_to_monitor=/mnt/media
```

3. Optionnaly, add a `udev` rule to fix the name of the `/dev/` device created when pluging in the usb cable from the Arduino.
Create a file `/etc/udev/rules.d/77-arduino.rules` with :
```text
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0001",  SYMLINK+="ttyArduino"
```
Next, reload the rules :  
```
sudo udevadm control --reload-rules
```
Unplug and plug in the Arduino.

If you have trouble with device busy or lock, add another `udev` rule to disable `ModemManager` for the Arduino (it thinks it is a modem). 
Add the following to the file `/etc/udev/rules.d/77-arduino.rules` :
```text
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0001", ENV{ID_MM_DEVICE_IGNORE}="1"
```
Reload the rules :  
```
sudo udevadm control --reload-rules
```
Unplug and plug in the Arduino.

### Arduino sketch

Open `sketch/sketch.ino` in the [Arduino IDE](https://www.arduino.cc/en/Main/Software), compiles it and uploads it to the Arduino.

## Hardware

## Relay card

The Arduino is connected to the relay card with the power supply and output pins.
It can be used to switch on of off a lamp.

![Arduino and relay card wiring](docs/relay_lamp.png)

## Temperature

The sketch is also able to handle thermal sensors DS18B20 connected to pin number 10.

![Arduino and relay card wiring](docs/ds18b20.png)

### Software

The perl script communicates through the emulated serial port with the Arduino with a simple text protocol to execute command in order to retrieve state, temperature and to switch on or off outputs. The Arduino waits for commands and executes them.

#### Arduino

Just load the sketch with the Arduino IDE, compile it and upload it.
Next, open a serial console.

On linux, you can use `picocom` :
```sh
picocom /dev/ttyACM0 -b 9600
```

##### Welcome message

Like a shell, a prompt listens for inputs. To display the list of available commands, type `h` : 
```sh
> h
    ___          _       _
   / _ \        | |     (_)
  / /_\ \_ __ __| |_   _ _ _ __   ___
  |  _  | '__/ _` | | | | | '_ \ / _ \
  | | | | | | (_| | |_| | | | | | (_) |
  \_| |_/_|  \__,_|\__,_|_|_| |_|\___/

Commands available :
  pin <pin number [4-8]> <0,1> - set pin value
  h|help                       - help
  s|io|status                  - i/o status
  t|temp                       - temperature
  p|pressure                   - pressure
  load1 <val>                  - set load (1 min) value (log scaled)
  load5 <val>                  - set load (5 min) value (log scaled)
  disk <val>                   - set disk value
```

##### Output controller

To display the state of outputs, type **s**
```sh
> s
I/O Status :
  2 => 0
  3 => 0
  4 => 0
  5 => 0
  6 => 0 (R)
  7 => 0 (R)
  8 => 0 (R)
  9 => 0 (R)
```
The output 2 to 9 are listed and they are all in low (0) state.
For my needs, outputs connected to relay are inverted. The are identified with the `(R)` symbol.

In order to switch an output on or off, the command is composed from the output number followed by the sign `=` and with `1` (high) or `0` (low).
eg : switch on the output n°2 :
```sh
> 2=1
  OK
```

Check the state :
```sh
> s
I/O Status :
  2 => 1
  3 => 0
  4 => 0
  5 => 0
  6 => 0 (R)
  7 => 0 (R)
  8 => 0 (R)
  9 => 0 (R)
```

To setup pins output for relay, you'll need to edit `sketch.ino` to change the array accordingly.
* `0` is normal output
* `1` is for inverted output

```C
// pin 6 - 9 are connected to relay
const int relays[NB_OUTPUT] = {0,0,0,0,0,0,1,1,1,1};
```

##### Temperature

In order to display the temperature, type **t**. The program look for any thermal sensors DS18B20 connected to it, for each found, it request the temperature and displays it in Celsius.
```sh
> t
Temperature :
ROM = [ 28 A 4C 3B 4 0 0 5B ] - Temperature : 12.69 °C
ROM = [ 28 6 AD 33 4 0 0 2D ] - Temperature : 15.94 °C
```

### Controller

The perl script handles the communication described above and offer a cli usage.

#### Usage

Usage :
```sh
> ./pin.pl
./pin.pl [-p dev] (<pin> <state> | status)
        <dev>   - device [/dev/ttyACM0]
        status  - retrieve the current status of the outputs
        <pin>   - pin number
        <action> - low|off / high|on / toggle
```

Display the state of output, like above :
```sh
> ./pin.pl status
Status
  2 => 1
  3 => 0
  4 => 0
  5 => 0
  6 => 0 (R)
  7 => 0 (R)
  8 => 0 (R)
  9 => 0 (R)
```

Switch on the output number 2
```sh
> ./pin.pl 2 on
```

toggle an output
```sh
> ./pin.pl 2 toggle
```

#### Temperature

This script retrieves the temperature of all DS18B20 sensors connected to the Arduino and logs it in a file named `temperature.<date>.log` in the folder `/var/lib/temperature/`. One line per invocation at the end of the file.
The date is formatted as `%Y-%m-%d`.
It also reads a configuration file `/etc/arduino.cfg` in order to provide a name for the 64 bit addresses of thermal sensors.
The config file looks like this :
```
sensor1=28 A 4C 3B 4 0 0 5B : ext. nord
sensor2=28 6 AD 33 4 0 0 2D : garage
```

and then the line added to the file is :
```
2015-10-13 00:00:05 : ext. nord=11.13 - garage=14.88 -
```

Usage :
```sh
> ./temperature.pl
./temperature.pl usage:
        -d : debug output
```

## License

Arduino-controller is distributed under [GPL v3 License](LICENSE).
