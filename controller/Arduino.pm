#!/usr/bin/perl 

package Arduino;

use Exporter;
use Device::SerialPort;
use Config::Simple;

@ISA = qw(Exporter);
@EXPORT = qw(open_serial empty_serial_buffer set_debug get_debug log_debug check_lock exec_cmd die_cleanly exit_cleanly delete_file signal_handler load_config serial_log serial_search);

# maximum number of loop to wait for locks to free
my $nb_max_locks_wait = 6;

use constant PROMPT => "> ";
use constant CONFIG_FILE => "/etc/arduino.cfg";

my $debug = 0;
my $port;
my $filename_lock;

#####################
# empty the serial buffer by reading all data available
sub open_serial {
    my $portName = shift;
    # Set up the serial port
    # 9600, 81N on the USB ftdi driver
    $port = Device::SerialPort->new($portName) || die_cleanly("Can't open $portName: $!");
    $port->databits(8);
    $port->baudrate(9600);
    $port->parity("none");
    $port->stopbits(1);
    $port->dtr_active(0);
    $port->read_const_time(500);
    $port->read_char_time(5);
    return $port;
}

#####################
# search the "> " prompt
sub empty_serial_buffer {
    serial_search( sub { } );
}

#####################
# with the given sub, search in the serial output
sub serial_search {
    my $search = shift;

    my $st = "";
    my $tEnd = time() + 2;
    while (time() < $tEnd) {
        my $byte = $port->read(1);
        if ($byte eq "") { next; }
        if (ord $byte == '10' || ord $byte == '13') {
            serial_log($st);
            $search->($st);
            $st = "";
            next;
        }
        $st .= $byte;
        if ($st eq PROMPT) {
            serial_log($st);
            last;
        }
    }
}

#####################
# 
sub serial_log {
    $msg = shift;

    log_debug("serial> $msg") unless $msg eq "";
}

#####################
# set debug (0 : no debug output, 1 : verbose output)
sub set_debug {
    $debug = shift;
}

#####################
# get debug state (0 : no debug output, 1 : verbose output)
sub get_debug {
    return $debug;
}

#####################
# log debug
sub log_debug {
    my $msg = shift;
    print "$msg\n" unless (!$debug);
}

#####################
# check if the number of lock is superior to the specified number and sleep
# if so, otherwise returns
# @param $lock : the lock to create 
# @param $locks : directory containing locks + basename of locks
# @param $nb_max_locks : max number of locks
sub check_lock {
    my $lock = shift;
    my $locks = shift;
    my $nb_max_locks = shift;

    $filename_lock = $lock;

    my $cpt = 0;
    my $is_locked = 0;
    do {
        $is_locked = 0;
        exec_cmd("touch $lock");

        my @markers = <$locks*>;
        if ($#markers >= $nb_max_locks) {
            log_debug("Ceil[$nb_max_locks] of locks reached : $locks => sleeping.");
            delete_file($lock);
            if ($cpt < $nb_max_locks_wait) { sleep(5); }
            $is_locked = 1;
            $cpt++;
        }
    } while ($is_locked && $cpt <= $nb_max_locks_wait);

    if ($cpt >= $nb_max_locks_wait) {
        die_cleanly("Waited to long for locks $locks to free");
    }
}

#####################
# execute the specified command 
# exit cleanly if something wrong happened
# @param $cmd : command to execute
sub exec_cmd {
    my $cmd = shift;
    log_debug("cmd : $cmd");
    my $ret = system( $cmd );
    if ($ret != 0) {
        die_cleanly("command failed : '$cmd' : $!");
    }
}

#####################
# Print an error message, delete temp file, close the connection to the
# database, remove the lock if it exits and exit
# @param $msg : error to display
sub die_cleanly {
    my $msg = shift;
    printf("### ERROR : $msg\n");
    exit_cleanly(1);
}

#####################
# Delete temp file, close the connection to the database, remove the lock if it
# exits and exit with the specified code
# @param $exit_status : exit code
sub exit_cleanly {
    my $exit_status = shift;

    $port->close if (defined $port);

# remove the lock file
    unlink($filename_lock) if (defined $filename_lock && -f $filename_lock);

    exit($exit_status);
}

#####################
# Delete file. If an error occurs, call die_cleanly
# @param $file
sub delete_file {
    my $file = shift;
    if (-f $file) {
        unlink($file) or die_cleanly("Cannot remove file '$file' : $!");
    }
}

#####################
# load the config
sub load_config {
    my $conf = new Config::Simple(CONFIG_FILE) or die_cleanly(Config::Simple->error());
    return $conf;
}

#####################
# signal handler
sub signal_handler {
    my $signame = shift;
    die_cleanly("Signal [$signame] catched");
}

1;
